<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://sciences.ucf.edu/
 * @since      1.0.0
 *
 * @package    Cos_Smd
 * @subpackage Cos_Smd/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Cos_Smd
 * @subpackage Cos_Smd/includes
 * @author     Jonathan Hendricker <jhendricker@gmail.com>
 */
class Cos_Smd_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
