<?php

/**
 * Fired during plugin activation
 *
 * @link       https://sciences.ucf.edu/
 * @since      1.0.0
 *
 * @package    Cos_Smd
 * @subpackage Cos_Smd/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cos_Smd
 * @subpackage Cos_Smd/includes
 * @author     Jonathan Hendricker <jhendricker@gmail.com>
 */
class Cos_Smd_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		// Require parent plugin
    	if ( ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) and current_user_can( 'activate_plugins' ) ) {
	        // Stop activation redirect and show error
	        wp_die('Sorry, but this plugin requires the Advanced Custom Fields plugin to be installed and active. <br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>');
	    }

	}

}
