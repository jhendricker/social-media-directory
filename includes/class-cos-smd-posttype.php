<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       https://sciences.ucf.edu/
 * @since      1.0.0
 *
 * @package    Cos_Smd
 * @subpackage Cos_Smd/includes
 */

if ( ! class_exists( 'COS_SMD_PostType' ) ) {
	class COS_SMD_PostType {		

		// **********************************************
		// * Custom Post Type for Social Media Directory
		// **********************************************
		public static function register_cos_smd_posttype() {
		  
		  $labels = array(
		    'name'                => _x('Social Media Directory', 'post type general name'),
		    'singular_name'       => _x('Social Media Directory Item', 'post type singular name'),
		    'add_new'             => _x('Add New Item', 'cos_smd_cpt'),
		    'add_new_item'        => __('Add New Directory Item'),
		    'edit_item'           => __('Edit Directory Item'),
		    'new_item'            => __('New Directory Item'),
		    'all_items'           => __('All Directory Items'),
		    'view_item'           => __('View Directory Item'),
		    'search_items'        => __('Search All Directory Items'),
		    'not_found'           => __('No Directory Items found.'),
		    'not_found_in_trash'  => __('No directory items found in Trash.'),
		    'parent_item_colon'   => '',
		    'menu_name'           => __('Social Media Directory'),
		  );

		  $args = array(
		    'labels'          => $labels,
		    'public'          => true,
		    'show_ui'         => true,
		    'capability_type' => 'post',
		    'hierarchical'    => true,
		    'rewrite'         => false,
		    'supports'        => array('custom-fields'),
		    'taxonomies'      => array('cos_smd_cat'),
		  );		  
		  register_post_type( 'cos_smd_cpt', $args );
		}
		

		// **********************************************
		// * Custom taxonomy for Social Media Directory
		// **********************************************
		public static function cos_smd_cat() {
		  // create a new taxonomy
		  $labels = array(
		    'name'              => _x( 'Directory Groups', 'taxonomy general name' ),
		    'singular_name'     => _x( 'Directory Group', 'taxonomy singular name' ),
		    'search_items'      => __( 'Search Directory Groups' ),
		    'all_items'         => __( 'All Directory Groups' ),
		    'parent_item'       => __( 'Parent Directory Group' ),
		    'parent_item_colon' => __( 'Parent Directory Group:' ),
		    'edit_item'         => __( 'Edit Directory Group' ),
		    'update_item'       => __( 'Update Directory Group' ),
		    'add_new_item'      => __( 'Add New Directory Group' ),
		    'new_item_name'     => __( 'New Directory Group Name' ),
		    'menu_name'         => __( 'Directory Groups' ),
		  );
		  $args = array(
		    'labels'        => $labels,
		    'sort'          => true,
		    'hierarchical'  => true,
		    'args'          => array( 'orderby' => 'term_order' ),
		    'query_var'     => true,
		    'rewrite'       => false, 
		  );
		  register_taxonomy( 'cos_smd_cat',  array('cos_smd_cpt'), $args );
		}
		

		// ***********************************
		// * Social Media Directory Shortcode
		// ***********************************
		public static function cos_smd_shortcode() {

		  ob_start();		  

		  // Display the Main Social Media Directory information
		  echo COS_SMD_PostType::social_media_directory_display();  

		  /* Loop through the other available Social Media Directory "Directory Groups" taxonomy entries. Display the entries for each "Directory Group".  Exclude the "main" directory group. */

		  // Get the ID for the "Main" Directory Group so it can be excluded
		  $mainID = "";
		  $mainID = get_term_by( 'slug', 'main', 'cos_smd_cat' );
		  $mainID = $mainID->term_id;

		  $dept_args = array(
		    'orderby'   =>  'slug',
		    'exclude'   =>  $mainID,
		  );
		  $directory_groups = get_terms( 'cos_smd_cat', $dept_args );

		  // Iterate through each "Directory Group"
		  foreach( $directory_groups as $directory_group ) :

		    echo COS_SMD_PostType::social_media_directory_display( $directory_group->name, $directory_group->slug );

		  endforeach;

		  return ob_get_clean();

		}
		

		// **********************************************
		// * Function to query and return the requested 
		// * Social Media Directory Group entries  
		// **********************************************
		public static function social_media_directory_display( $directory_group_name = 'Main', $directory_group_slug = 'main' ) {
		  
		  /* Grabbing the posts in the "Main" taxonomy for the Social Media Directory CPT */
		  $mainArgs = array(
		    'post_type'       =>  "cos_smd_cpt",
		    'tax_query'       =>  array(
		      array(
		        'taxonomy'    =>  'cos_smd_cat',
		        'field'       =>  'slug',
		        'terms'       =>  $directory_group_slug,
		        ),
		    ),
		    'posts_per_page'  => -1,
		    'orderby'         => 'menu_order',
		    'order'           => 'ASC',
		  );
		  $mainQuery = new WP_Query( $mainArgs );

		  ob_start();

		  if( $mainQuery->have_posts()) : 

		    if($directory_group_slug === 'main'){
		      $section_header =  "<div class='smd_section'>
		            <div class='smd_area_title'></div>
		            <div class='smd_social_box'>
		              <div class='smd_icon smd_facebook'></div>
		              <div class='smd_icon smd_twitter'></div>
		              <div class='smd_icon smd_youtube'></div>
		              <div class='smd_icon smd_instagram'></div>
		              <div class='smd_icon smd_linkedin'></div>
		            </div>
		          </div>";
		    } else{
		      $section_header =  "<div class='smd_section'>
		            <div class='smd_area_title smd_title_full'>$directory_group_name</div>
		            <div class='smd_social_box'></div>
		          </div>";
		    }
		    echo $section_header;

		    while( $mainQuery->have_posts()) : $mainQuery->the_post();

		      $smd_name       = get_field('cos_smd_name');
		      $smd_link       = get_field('cos_smd_weblink');
		      $smd_social   = array(
		        'facebook'   => get_field('cos_smd_facebook'),
		        'twitter'    => get_field('cos_smd_twitter'),
		        'youtube'    => get_field('cos_smd_youtube'),
		        'instagram'  => get_field('cos_smd_instagram'),
		        'linkedin'   => get_field('cos_smd_linkedin'),
		      );    

		    ?>
		      <div class='smd_row'>
		        <div class='smd_row_title'><?php echo (!empty($smd_link) ? "<a href='$smd_link' target='_blank'>$smd_name</a>" : $smd_name ); ?></div>
		        <div class="smd_social_box">
		        <?php 
		          // Loop through the Social Media types
		          foreach ($smd_social as $social_site => $value): ?>
		            <div class='smd_<?= $social_site; ?>'><?php 
		              if(!empty($value)): ?><a href='<?= $value; ?>' target='_blank' ><i class="fa fa-check"></i></a>
		            <?php endif; ?>
		            </div> 
		            <?php 
		          endforeach;
		        ?>      
		        </div>
		      </div>
		    <?php
		    endwhile;  

		    endif;  
		    
		    wp_reset_query();  

		    return ob_get_clean();
		}

		// *****************************************************
		// * Custom Titles for CPTs that don't use Title field
		// *****************************************************
		public static function custom_titles( $title ) {

			$postID = get_the_ID();
			$postType = get_post_type( $postID );

			if( $postType == 'cos_smd_cpt') {    
				$title = $_POST['acf']['field_5554da92ac337'];
			}

			return $title;
		}
	
	}
}
