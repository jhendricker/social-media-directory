=== Plugin Name ===
Contributors: jhendricker
Tags: social media directory
Requires at least: 4.5
Tested up to: 4.9
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds the ability to create and display a social media directory

== Description ==

This plugin adds the ability to create and display a social media directory for multiple areas using shortcode. It requires the installation of Advanced Custom Fields and the importing of the provided COS_SMD_ACF_Group.json file.

== Installation ==

= Manual Installation =
1. Upload the plugin files (unzipped) to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Requires the installation of the Advanced Custom Fields or Advanced Custom Fields Pro plugin.
3. Activate the plugin through the 'Plugins' menu in WordPress.
4. Place `[cos_social_media_directory]` shortcode in any of your posts/pages to display the full social media directory.
5. Create a Directory Group called Main to put any groups into. You can create additional groups to better organize the Directory Items

== Installation Requirements ==
It requires the installation of Advanced Custom Fields and the importing of the provided COS_SMD_ACF_Group.json file.

== Frequently Asked Questions ==
= What is the shortcode? =
`[cos_social_media_directory]`

= Why are no entries showing up? =

The plugin cycles through entries that are assigned to Directory Groups. Items must be assigned to groups to properly show up. Try creating a group called Main and adding items to it.

= Can I group directory items together? =

Yes, you can create Directory Groups and assign Directory Items to them.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0.0 =
* Initial release

== Upgrade Notice ==

n/a