<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://sciences.ucf.edu/
 * @since             1.0.1
 * @package           Cos_Smd
 *
 * @wordpress-plugin
 * Plugin Name:       COS Social Media Directory
 * Plugin URI:        https://sciences.ucf.edu/
 * Description:       Contains shortcode for displaying social media directory information for the College of Sciences and its varoius departments, schools, centers, etc. Shortcode is [cos_social_media_directory]
 * Version:           1.0.0
 * Author:            Jonathan Hendricker
 * Author URI:        https://sciences.ucf.edu/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cos-smd
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'COS_SOCIAL_MEDIA_DIRECTORY_PLUGIN', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cos-smd-activator.php
 */
function activate_cos_smd() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cos-smd-activator.php';
	Cos_Smd_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cos-smd-deactivator.php
 */
function deactivate_cos_smd() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cos-smd-deactivator.php';
	Cos_Smd_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cos_smd' );
register_deactivation_hook( __FILE__, 'deactivate_cos_smd' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cos-smd.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cos_smd() {

	$plugin = new Cos_Smd();
	$plugin->run();

}
run_cos_smd();


if ( ! function_exists( 'cos_smd_init' ) ) {
	function cos_smd_init() {

		add_action( 'init', array( 'COS_SMD_PostType', 'register_cos_smd_posttype' ), 10, 0 );	
		add_action( 'init', array( 'COS_SMD_PostType', 'cos_smd_cat' ), 10, 0 );		

		add_filter('title_save_pre', array('COS_SMD_PostType', 'custom_titles') );		

		if ( !shortcode_exists( 'cos_social_media_directory' )) {
			add_shortcode('cos_social_media_directory', array('COS_SMD_PostType', 'cos_smd_shortcode') );			
		}
	}
	add_action( 'plugins_loaded', 'cos_smd_init' );
}

